"use strict";
const crypto = require('crypto');
require('dotenv-safe').config();
/**
 * @typedef {import('moleculer').ServiceSchema} ServiceSchema Moleculer's Service Schema
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

/** @type {ServiceSchema} */
module.exports = {
	name: "accounts",

	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		getAccounts: {
			rest: {
				method: "GET",
        path: "/",
			},
      params: {
				phoneNumber: "string",
      },
			async handler(ctx) {
				const { headers } = ctx.options.parentCtx.params.req;
				const infobipAccountId = headers['x-ib-exchange-req-accountkey'];
				const signature = headers['x-ib-exchange-req-signature'];
				const timestamp = headers['x-ib-exchange-req-timestamp'];

				if(signature !== this.hash(`${timestamp}${JSON.stringify(ctx.params)}`))
					throw new Error('Invalid signature');

        const credentials = await ctx.call("credentials.getCredentialsByInfobipAccountId", {
					infobipAccountId,
				});

				const result = await fetch("https://api.orangepill.cloud/v1/accounts", {
					method: "GET",
					headers: {
						"Content-Type": "application/json",
						"x-api-key": btoa(`${credentials.realm}:${credentials.username}:${credentials.password}`),
					},
				})
				.then(response => response.json());

				return result;
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		hash(valueToHash) {
			return crypto
			.createHmac('sha256', process.env.INFOBIP_EXCHANGE_SIGNING_SECRET)
			.update(valueToHash)
			.digest('hex');
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
