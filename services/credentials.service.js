"use strict";

/**
 * @typedef {import('moleculer').ServiceSchema} ServiceSchema Moleculer's Service Schema
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */
const credentialsStore = [];

/** @type {ServiceSchema} */
module.exports = {
	name: "credentials",

	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		saveCredentials: {
			rest: {
        method: "POST",
        path: "/save",
      },
      body: {
        infobipAccountId: "string",
        realm: "string",
        username: "string",
        password: "string",
      },
			/** @param {Context} ctx  */
			async handler(ctx) {
				if (credentialsStore.findIndex(credentials => credentials.infobipAccountId === ctx.params.infobipAccountId) === -1)
					credentialsStore.push(ctx.params);
			}
		},
		getCredentialsByInfobipAccountId: {
			rest: {
				method: "GET",
				path: "/get/:infobipAccountId",
			},
			params: {
				infobipAccountId: "string"
			},
			/** @param {Context} ctx  */
			async handler(ctx) {
				return credentialsStore.filter(credentials => credentials.infobipAccountId === ctx.params.infobipAccountId).pop();
			},
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
