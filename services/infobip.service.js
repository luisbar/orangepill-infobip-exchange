"use strict";
require('dotenv-safe').config();

/**
 * @typedef {import('moleculer').ServiceSchema} ServiceSchema Moleculer's Service Schema
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

/** @type {ServiceSchema} */
module.exports = {
	name: "infobip",

	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		clientCode: {
			rest: {
				method: "GET",
        path: "/client-code",
			},
			async handler(ctx) {
        const clientCodeQueryParams = new URLSearchParams({
          response_type: 'code',
          client_id: process.env.INFOBIP_EXCHANGE_CLIENT_ID,
          redirect_uri: 'https://9cbc-181-114-103-137.ngrok.io/api/infobip/auth',
        });

        ctx.meta.$statusCode = 302;
        ctx.meta.$location = `https://portal.infobip.com/conversations/api/amg/exchange/1/oauth/authorize?${clientCodeQueryParams}`;
			}
		},
		auth: {
			rest: {
				method: "GET",
        path: "/auth",
			},
      params: {
        code: 'string',
      },
			async handler(ctx) {
        const authenticationBody = new URLSearchParams({
          client_id: process.env.INFOBIP_EXCHANGE_CLIENT_ID,
          client_secret: process.env.INFOBIP_EXCHANGE_CLIENT_SECRET,
          code: ctx.params.code,
          grant_type: 'authorization_code',
        });

        const result = await fetch('https://portal.infobip.com/conversations/api/amg/exchange/1/oauth/token', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: authenticationBody
        })
        .then(response => response.json())
        .catch(error => {
          console.error(error);
        });

        ctx.meta.$statusCode = 302;
        ctx.meta.$location = `/?infobipAccountId=${result.accountKey}`;
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
