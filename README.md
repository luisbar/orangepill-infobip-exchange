[![Moleculer](https://badgen.net/badge/Powered%20by/Moleculer/0e83cd)](https://moleculer.services)

# orangepill-infobip-exchange
This is a [Moleculer](https://moleculer.services/)-based microservices project. Generated with the [Moleculer CLI](https://moleculer.services/docs/0.14/moleculer-cli.html).

## Usage
Start the project with `npm run dev` command. 
After starting, open the http://localhost:3000/ URL in your browser. 
On the welcome page you can test the generated services via API Gateway and check the nodes & services.

In the terminal, try the following commands:
- `nodes` - List all connected nodes.
- `actions` - List all registered service actions.
- `call greeter.hello` - Call the `greeter.hello` action.
- `call greeter.welcome --name John` - Call the `greeter.welcome` action with the `name` parameter.

## Useful links

* Moleculer website: https://moleculer.services/
* Moleculer Documentation: https://moleculer.services/docs/0.14/

## NPM scripts

- `npm run dev`: Start development mode (load all services locally with hot-reload & REPL)
- `npm run start`: Start production mode (set `SERVICES` env variable to load certain services)
- `npm run cli`: Start a CLI and connect to production. Don't forget to set production namespace with `--ns` argument in script
- `npm run lint`: Run ESLint
- `npm run ci`: Run continuous test mode with watching
- `npm test`: Run tests & generate coverage report
- `npm run dc:up`: Start the stack with Docker Compose
- `npm run dc:down`: Stop the stack with Docker Compose

## How to configure [Infobip Exchange](https://www.infobip.com/docs/integrations/exchange-developer#set-up-account-access) for creating an app with [Orangepill](https://docs.orangepill.cloud)

- First you have to create an account in [Orangepill Cloud](https://orangepill.cloud/user/register)
- Then, go to `Publish` section in the [Infobip Portal](https://portal.infobip.com/exchange/partners)
  - Create an app
    - Set the name
    - Choose `answers` and `whatsapp`
    - Set the following manifest
      ```json
      {
        "functions":[
            {
              "name":"Get accounts",
              "description":"Get accounts",
              "method":"GET",
              "uri":"https://YOUR_DOMAIN/api/accounts",
              "inSchema":{
                  "type":"object",
                  "required":[
                    "phoneNumber"
                  ],
                  "properties":{
                    "phoneNumber":{
                        "type":"string",
                        "title":"Phone number"
                    }
                  }
              },
              "outSchema":{
                  "type":"object",
                  "required":[
                    "rows"
                  ],
                  "properties":{
                    "rows":{
                        "type":"array",
                        "items":{
                          "type":"object",
                          "required":[
                              "asset",
                              "chain",
                              "holder",
                              "currency",
                              "type"
                          ],
                          "properties":{
                              "asset":{
                                "type":"string"
                              },
                              "chain":{
                                "type":"string"
                              },
                              "holder":{
                                "type":"string"
                              },
                              "currency":{
                                "type":"string"
                              },
                              "type":{
                                "type":"string"
                              }
                          }
                        }
                    }
                  }
              }
            }
        ]
      }
      ```
    - Set settings url 
    - Set redirection url
    - Settings url should request a client code by doing the following request
        ```javascript
        const clientCodeQueryParams = new URLSearchParams({
          response_type: 'code',
          client_id: process.env.INFOBIP_EXCHANGE_CLIENT_ID,
          redirect_uri: 'https://YOUR_DOMAIN/api/infobip/auth',
        });

        ctx.meta.$statusCode = 302;
        ctx.meta.$location = `https://portal.infobip.com/conversations/api/amg/exchange/1/oauth/authorize?${clientCodeQueryParams}`;
        ```
      - And Infobip will invoke your redirection url
        - It should get the token by doing the following request
          ```javascript
            const authenticationBody = new URLSearchParams({
            client_id: process.env.INFOBIP_EXCHANGE_CLIENT_ID,
            client_secret: process.env.INFOBIP_EXCHANGE_CLIENT_SECRET,
            code: ctx.params.code,
            grant_type: 'authorization_code',
          });
  
          const result = await fetch('https://portal.infobip.com/conversations/api/amg/exchange/1/oauth/token', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: authenticationBody
          })
          .then(response => response.json())
          ```
        - And finally it should redirect to a form (for saving Orangepill credentials) by sending the Infobip account id
    - Go to Answers and build your Chatbot
      - You will see the app that you created as a block for interacting with it
    - Here you have an [example](/screenshots/example.mov)
    - Here you have a diagram of the explained flow
      ![diagram](/screenshots/flow.png)